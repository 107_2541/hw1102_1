package edu.cyut.javaclass;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int [][] arrayA = {{1, 2,  3},
                {4, 5,  6}};
        int [][] arrayB = {{7, 8,  9},
                {10, 11, 12}};
        int [][] arrayC = {{0, 0, 0},
                {0, 0, 0}};
        int i,j,k;

        System.out.print("ArrayA : ");
        for(i=0; i<2; i++) {
            for(j=0; j<3; j++) {
                if(arrayA[i][j]<10)
                    System.out.print("0"+arrayA[i][j] + " ");//如果數字為個位數前面加0輸出
                else
                    System.out.print(arrayA[i][j] + " ");//直接輸出
            }
        }
        System.out.println("");
        System.out.print("ArrayB : ");
        for(i=0; i<2; i++) {
            for(j=0; j<3; j++) {
                if(arrayB[i][j]<10)
                    System.out.print("0"+arrayB[i][j] + " ");//如果數字為個位數前面加0輸出
                else
                    System.out.print(arrayB[i][j] + " ");//直接輸出
            }
        }
        System.out.println("");
        System.out.print("ArrayC : ");
        for(i=0; i<2; i++) {
            for(j=0; j<3; j++) {
                arrayC[i][j] = arrayA[i][j] + arrayB[i][j];//arrayC等於arrayA加arrayB
                if(arrayC[i][j]<10)
                    System.out.print("0"+arrayC[i][j] + " ");//如果數字為個位數前面加0輸出
                else
                    System.out.print(arrayC[i][j] + " ");//直接輸出
            }
        }
//
    }
}
